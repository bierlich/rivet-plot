from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np
import yoda

hist = yoda.Histo2D(20,-10,10,20,-10,10,"/2dtest/hist","hist")
x = np.random.normal(0.,4., 10000)
y = np.random.normal(0.,4., 10000)

for xx,yy in zip(x,y):
    hist.fill(xx,yy)

yoda.write([hist],"3dtest.yoda")

