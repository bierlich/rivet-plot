To use:

1. Clone the repo on a machine with Rivet already installed.
2. Run the env.sh script
3. Two test-yoda files are included - you may need to replace local .plot files 
with those supplied to avoid latex errors.

Normal plotting:
./rivet-mkhtml Rivet.yoda:LegendLabel --matplotlib
Using custom style file:
./rivet-mkhtml Rivet.yoda:LegendLabel --matplotlib --mplstyle=mystylefile
Styles can be stacked, with the first one applied first etc:
./rivet-mkhtml Rivet.yoda:LegendLabel --matplotlib --mplstyle=mystylefile1 --mplstyle=mystylefile2 ...

