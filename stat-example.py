import rivet, yoda, sys, os
import matplotlib.pyplot as plt
import numpy as np
from scipy import optimize, integrate
import rivetcmp as rcmp
from matplotlib.colors import ListedColormap

# Example of how to use rivetcmp for statistics.
# Extract data and MC from a rivet run, do some fitting,
# and plot it rivet style.

# Fit function, Levy-Tsallis. Parameters are:
# p[0] = dN/dy
# p[1] = n
# p[2] = C
# From https://arxiv.org/pdf/1101.4110.pdf

mzero = 0
def levyt(x, p0, p1, p2):
    dndy = p0
    n = p1
    C = p2
    nc = n*C
    d1 = (n - 1)*(n - 2)
    d2 = nc*(nc + mzero*(n - 2))
    mt = np.sqrt(np.power(x,2.) + np.power(mzero,2.))
    d3 = 1 + (mt - mzero)/nc
    return x*dndy*(d1/d2)*np.power(d3,-n)

# Use the Rivet style.
plt.style.use('rivet')

# Extract data and MC.
# TODO: Some of the gory details should be hidden away.
# Ideally just a single call.
f1 = ['mctest.yoda']
histname = '/ALICE_2016_I1471838:cent=GEN/d18-x01-y01'
data, mc = rcmp.getHistos(f1,None)
data = rcmp.getRivetRefData(None,['ALICE_2016_I1471838'])
dataScatter = data[histname.replace(':cent=GEN','')]
mcScatter = mc[f1[0]][histname][0]

# Make a figure and axes objects.
fig, ax1, ax2 = rcmp.makefig(True)

# Add data points to plot
rcmp.addToAx(ax1, dataScatter, [], [], True, True, 'ALICE Data')
rcmp.addRatioToAx(histname, ax2, dataScatter, dataScatter, [], [], True, True)

# Make a plot-object from the default .plot file for this histogram.
# TODO: Sensible default for plotparser and colormap.
plotparser = rivet.mkStdPlotParser([os.getcwd()],[])
plot, special = rcmp.plotSpec(histname.replace(':cent=GEN',''), False, plotparser,rcmp.Plot())
cmap = ListedColormap(['blue', 'blue'])
colIter = iter(cmap([0, 1]))

# Add MC and ratio plot to axes. TODO: Make call less gory.
rcmp.addToAx(ax1, mcScatter, plot, colIter, True, False, 'Monte Carlo')
rcmp.addRatioToAx(histname, ax2, mcScatter, dataScatter, [], colIter, True)

# Add plot attributes from the default plot file.
rcmp.addPlotAttributes(histname, plot, False, ax1, ax2)

# Extract x and y coordinates for MC.
x, y, _, _ = rcmp.getXY(mcScatter)
xd, yd, _, _ = rcmp.getXY(dataScatter)

# Fitting and plotting the fit is pure Python.
# Fit with Levy-Tsallis function.
p0 = [0.5, 6.0, 0.1]
pmc, cov = optimize.curve_fit(levyt, x, y, p0=p0)
pdata, cov = optimize.curve_fit(levyt, xd, yd, p0=p0)
x0 = np.linspace(0.0,8.,100)
mass = 0.497
mzero = mass
ax1.plot(x0, levyt(x0,*pmc),color='red',label='MC Levy-Tsallis fit')
ax1.plot(x0, levyt(x0,*pdata),color='green',label='Data Levy-Tsallis fit')

ax1.legend()
ax1.set_xlim(0,8)
ax2.set_ylim(0.5,1.5)
ax1.set_yscale('log')
plt.show()
