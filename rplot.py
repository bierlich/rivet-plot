import rivet, yoda, sys, os
import matplotlib.pyplot as plt
import numpy as np
from scipy import optimize, integrate
import matplotlib.gridspec as gridspec

def getData(histname,analysis="ALICE_2016_I1471838"):
    data = yoda.core.read(analysis+".yoda")
    h = data["/REF/"+analysis+"/d"+histname+"-x01-y01"]
    x = []
    y = []
    err = []
    for b in h:
        x.append(b.x)
        y.append(b.y)
        err.append(b.yMax - b.yMin)
    return np.array(x), np.array(y), err

def getMC(histname,fname,analysis = "/ALICE_2016_I1471838:cent=GEN"):
    data = yoda.core.read(fname)
    h = []
    if len(histname) == 2:
        h = data[analysis+"/d"+histname+"-x01-y01"]
    else:
        h = data[analysis+"/"+histname]
    x = []
    y = []
    err = []
    for b in h:
        x.append(b.xMid)
        try:
            y.append(b.height)
            err.append(b.heightErr)
        except:
            y.append(b.mean)
            err.append(b.stdErr)
    return np.array(x), np.array(y), err


def getXY(h):

    x = [b.x for b in h]
    y = [b.y for b in h]
    xem = [b.xErrs.minus for b in h]
    xep = [b.xErrs.plus for b in h]
    yem = [b.yErrs.minus for b in h]
    yep = [b.yErrs.plus for b in h]
    return x, y, [xem, xep], [yem,yep]

f1 = 'ALICE_2010_S8625980_d03-x01-y01.dat'

histos = rivet-cmphistos("yodafile")
f, ax1, ax2 = histos["/NAME/d01-x01"]

yodafile = yoda.read(f1)
xd, yd, xde, yde = getXY(yodafile["/ALICE_2010_S8625980/d03-x01-y01"])
xp, yp, xpe, ype = getXY(yodafile["/Rivet.yoda/ALICE_2010_S8625980/d03-x01-y01"])

fig = plt.figure()
spec = gridspec.GridSpec(ncols=1,nrows=2,height_ratios=[3,1])
ax1 = fig.add_subplot(spec[0])
ax2 = fig.add_subplot(spec[1],sharex=ax1)
plt.setp(ax1.get_xticklabels(), visible=False)
ax1.errorbar(xd, yd, xerr=xde, yerr=yde, fmt='o', color='black',label="Data")
ax1.errorbar(xp, yp, xerr=xpe, yerr=ype, fmt='-', label="Rivet")

ratio = [y1/y2 for y1, y2 in zip(yp,yd)]
ax2.plot(xd, ratio)

ax1.set_yscale('log')

ax2.set_ylabel('MC / Data')
ax1.legend()
plt.show()

