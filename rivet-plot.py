#!/usr/bin/env python
from rivetcmp import *
import rivet
import matplotlib.pyplot as plt
import numpy as np
from scipy import optimize, integrate
import matplotlib.gridspec as gridspec
import matplotlib.colors as colors
import os
import logging

if __name__ == '__main__':

    ## Command line parsing
    import argparse
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument("ARGS", nargs="*")
    parser.add_argument('-o', '--outdir', dest='OUTDIR',
                        default='.', help='write data files into this directory')
    parser.add_argument("--hier-out", action="store_true", dest="HIER_OUTPUT", default=False,
                        help="write output dat files into a directory hierarchy which matches the analysis paths")
    parser.add_argument('--plotinfodir', dest='PLOTINFODIRS', action='append',
                        default=['.'], help='directory which may contain plot header information (in addition '
                        'to standard Rivet search paths)')
    parser.add_argument("--no-rivet-refs", dest="RIVETREFS", action="store_false",
                        default=True, help="don't use Rivet reference data files")
    # parser.add_argument("--refid", dest="REF_ID",
    #                   default="REF", help="ID of reference data set (file path for non-REF data)")
    parser.add_argument("--reftitle", dest="REFTITLE",
                        default='Data', help="Reference data legend entry")
    parser.add_argument("--pwd", dest="PATH_PWD", action="store_true", default=False,
                        help="append the current directory (pwd) to the analysis/data search paths (cf. $RIVET_ANALYSIS/DATA_PATH)")
    stygroup = parser.add_argument_group("Plot style")
    stygroup.add_argument("--linear", action="store_true", dest="LINEAR",
                          default=False, help="plot with linear scale")
    stygroup.add_argument("--errs", "--mcerrs", "--mc-errs", action="store_true", dest="MC_ERRS",
                          default=False, help="show vertical error bars on the MC lines")
    stygroup.add_argument("--no-ratio", action="store_false", dest="RATIO",
                          default=True, help="disable the ratio plot")
    stygroup.add_argument("--rel-ratio", action="store_true", dest="RATIO_DEVIATION",
                          default=False, help="show the ratio plots scaled to the ref error")
    stygroup.add_argument("--no-plottitle", action="store_true", dest="NOPLOTTITLE",
                          default=False, help="don't show the plot title on the plot "
                          "(useful when the plot description should only be given in a caption)")
    stygroup.add_argument("--style", dest="STYLE", default="default",
                          help="change plotting style: default|bw|talk")
    stygroup.add_argument("-c", "--config", dest="CONFIGFILES", action="append", default=["~/.make-plots"],
                          help="additional plot config file(s). Settings will be included in the output configuration.")
    stygroup.add_argument("--remove-options", help="remove options label from legend", dest="REMOVE_OPTIONS",
                          action="store_true", default=False)
    stygroup.add_argument("--cmap", help="set matplotlib color map.", dest="CMAP",
                          default="default")
    stygroup.add_argument("--mplstyle", help="add a(nother) matplotlib style.", dest="MPLSTYLE", action="append",
                          default=[])
    selgroup = parser.add_argument_group("Selective plotting")
    selgroup.add_argument("-m", "--match", action="append",
                          help="only write out histograms whose $path/$name string matches these regexes. The argument "
                          "may also be a text file.",
                          dest="PATHPATTERNS")
    selgroup.add_argument("-M", "--unmatch", action="append",
                          help="exclude histograms whose $path/$name string matches these regexes",
                          dest="PATHUNPATTERNS")
    verbgroup = parser.add_argument_group("Verbosity control")
    verbgroup.add_argument("-v", "--verbose", action="store_const", const=logging.DEBUG, dest="LOGLEVEL",
                           default=logging.INFO, help="print debug (very verbose) messages")
    verbgroup.add_argument("-q", "--quiet", action="store_const", const=logging.WARNING, dest="LOGLEVEL",
                           default=logging.INFO, help="be very quiet")
    args = parser.parse_args()
    
    # Output configuration.
    logging.basicConfig(level=args.LOGLEVEL, format="%(message)s")

    ## Add pwd to search paths
    if args.PATH_PWD:
        rivet.addAnalysisLibPath(os.path.abspath("."))
        rivet.addAnalysisDataPath(os.path.abspath("."))

    ## Split the input file names and the associated plotting options
    ## given on the command line into two separate lists
    filelist, plotoptions = parseArgs(args.ARGS)

    ## Remove the PLOT dummy file from the file list
    if "PLOT" in filelist:
        filelist.remove("PLOT")

    ## Check that the files exist
    for f in filelist:
        if not os.access(f, os.R_OK):
            logging.error("Error: cannot read from %s" % f)
            sys.exit(1)

    ## Read the .plot files
    plotdirs = args.PLOTINFODIRS + [os.path.abspath(os.path.dirname(f)) for f in filelist]
    plotparser = rivet.mkStdPlotParser(plotdirs, args.CONFIGFILES)

    ref, mc = getHistos(filelist, args)
    hpaths, h2ds, anas = getHistoLists(mc)
    ref = getRivetRefData(args,anas)

    for a in anas:
        if not os.path.exists(a):
            os.makedirs(a)
    plt.style.use('rivet')
    for sty in args.MPLSTYLE:
        plt.style.use(sty)
    # Make a good default color map.
    # TODO: Instead of hard-coding, make this take a cycler set in 
    # style file instead - but how?
    from matplotlib.colors import ListedColormap
    rivetCmap = ListedColormap(['red', 'blue', 'green', 'yellow', 'magenta', 'black', 'brown', 'pink', 'cyan', 'teal', 'orange'])
    logging.info("== Making "+str(len(hpaths))+" plots with matplotlib (experimental feature) ==")
    for hpath in hpaths:
        is2d = (hpath in h2ds)
        showratio = args.RATIO and not is2d
        plot, special = plotSpec(hpath, is2d, plotparser, plotoptions, args)
        # Get the color map
        cmap = rivetCmap
        if args.CMAP != "default":
            plt.set_cmap(args.CMAP)
            cmap = plt.get_cmap()
        colIterPlot = iter(cmap([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]))
        colIterRatio = iter(cmap([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]))
        if is2d:
            from mpl_toolkits import mplot3d
            fig = plt.figure()
            ax1 = plt.axes(projection='3d')
            for f in filelist:
                h_mc = mc[f][hpath][0]
                surf = add3dToAx(hpath, ax1, h_mc, plot, plotoptions, cmap)
                fig.colorbar(surf, shrink=0.5, aspect=5)
                addPlotAttributes(hpath, plot, is2d, ax1)
        else:
            fig, ax1, ax2 = makefig(showratio)
            ax2.set_ylim(0.5, 1.5)
            if hpath in ref:
                h_ref = ref[hpath]
                addToAx(ax1, h_ref, plotoptions, [], True, True)
                if showratio:
                    addRatioToAx(hpath, ax2, h_ref, h_ref, plotoptions, [], True, True)
            for f in filelist:
                h_mc = mc[f][hpath][0]
                colIterPlot = addToAx(ax1, h_mc, plotoptions[f], colIterPlot, args.MC_ERRS)
                if showratio:
                    colIterRatio = addRatioToAx(hpath, ax2, h_mc, h_ref, plotoptions[f], colIterRatio)
                addPlotAttributes(hpath, plot, is2d, ax1, ax2 )
        logging.info("Plotting "+hpath+" ("+str(hpaths.index(hpath))+"/"+str(len(hpaths))+")")
        plt.subplots_adjust(wspace = 0, hspace = 0)
        try:
            fig.savefig(hpath.strip("/")+".pdf")
            fig.savefig(hpath.strip("/")+".png")
        except Exception as e:
            logging.error("Error saving figures. Plot: "+hpath)
            logging.error(str(e))
        #plt.show()
        plt.close()
